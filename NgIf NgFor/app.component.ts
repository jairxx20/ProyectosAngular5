import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  activar:boolean = true;
  Paises=['Peru','Ecuador','Colombia'];

  mostrar()
  {
    this.activar = !this.activar;
  }
}
